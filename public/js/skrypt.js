/* jshint browser: true, globalstrict: true, devel: true */
/* global io: false */
/*global io: false, $: false*/
"use strict";

$(window).load(function (event) {
    var status = $("#status")[0];
    var open = $("#open")[0];
    var close = $("#close")[0];
    var send = $("#send")[0];
    var text = $("#text")[0];
    var message = $("#message")[0];
    var socket;
    var username;

    status.textContent = "Brak połącznia";

    $("#close").prop('disabled', true);
    $("#send").prop('disabled', true);

    $("#open").click(function (event) {
        $("#open").prop('disabled', true);
        if (!socket || !socket.connected) {
            socket = io({
                forceNew: true
            });
        }

        socket.on('connect', function () {

            socket.emit('adduser', prompt("Podaj login: "));
            close.disabled = false;
            send.disabled = false;
            status.src = "img/bullet_green.png";
            console.log('Nawiązano połączenie przez Socket.io');
        });

        socket.on('disconnect', function () {
            open.disabled = false;
            status.src = "img/bullet_red.png";
            console.log('Połączenie przez Socket.io zostało zakończone');
        });

        socket.on("error", function (err) {
            message.textContent = "Błąd połączenia z serwerem: '" + JSON.stringify(err) + "'";
        });

        socket.on("aqq", function (data) {
            message.textContent = "Serwer: " + data;
        });

        socket.on('updatechat', function (username, msg) {
            $('#message').append($('<p>').text(username + ": " + msg));
        });

        socket.on('updateusers', function (data) {
            $('#users').empty();
            $.each(data, function (key, value) {
                $('#users').append('<div>' + key + '</div>');
            });
        });

    });

    $("#close").click(function (event) {
        $("#close").prop('disabled', true);
        $("#send").prop('disabled', true);
        $("#open").prop('disabled', false);
        message.textContent = "";
        socket.io.disconnect();
        console.dir(socket);
    });

    $("#send").click(function (event) {

        socket.emit('sendchat', $('#text').val());
        $('#text').val('');
        return false;
    });

});