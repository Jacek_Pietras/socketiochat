/* jshint node: true */
var express = require('express');
var moragn = require("morgan");
var app = express();
var httpServer = require("http").Server(app);
var io = require("socket.io")(httpServer);


var port = process.env.PORT || 3000;

app.use(moragn("dev"));
app.use('/lib/', express.static(__dirname + '/bower_components/jquery/dist/'));
app.use(express.static(__dirname + '/public'));

var usernames = {};

io.sockets.on("connection", function (socket) {

    console.log('Użytkownik dołączył do czatu');

    socket.on('sendchat', function (msg) {
        io.emit('updatechat', socket.username, msg);
    });

    socket.on('adduser', function (username) {

        socket.username = username;

        usernames[username] = username;

        socket.emit('updatechat', 'SERVER', 'Zostałeś połączony z czatem');

        socket.broadcast.emit('updatechat', 'SERVER', username + ' dołączył do czatu');

        io.sockets.emit('updateusers', usernames);
    });

    socket.on('disconnect', function () {

        delete usernames[socket.username];

        io.sockets.emit('updateusers', usernames);

        socket.broadcast.emit('updatechat', 'SERVER', socket.username + ' opuścił czat');
    });

    socket.on("error", function (err) {
        console.dir(err);
    });
});

httpServer.listen(port, function () {
    console.log('Serwer HTTP działa na porcie ' + port);
});